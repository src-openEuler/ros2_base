# ros2_base

#### Description
The basic software package of ros2.

#### Software Architecture
Software architecture description

https://github.com/ros2-gbp/variants-release.git

input:
```
./
├── ament_cmake_ros
├── common_interfaces
├── console_bridge_vendor
├── eigen3_cmake_module
├── example_interfaces
├── geometry2
├── launch
├── launch_ros
├── libyaml_vendor
├── message_filters
├── mimick_vendor
├── orocos_kinematics_dynamics
├── performance_test_fixture
├── python_cmake_module
├── rcl
├── rclcpp
├── rcl_interfaces
├── rcl_logging
├── rclpy
├── rcpputils
├── rcutils
├── realtime_support
├── rmw
├── rmw_connext
├── rmw_cyclonedds
├── rmw_dds_common
├── rmw_fastrtps
├── rmw_implementation
├── ros1_bridge
├── ros2cli
├── ros2cli_common_extensions
├── rosbag2
├── rosidl
├── rosidl_dds
├── rosidl_defaults
├── rosidl_python
├── rosidl_runtime_py
├── rosidl_typesupport
├── rosidl_typesupport_connext
├── rosidl_typesupport_fastrtps
├── ros_testing
├── rpyutils
├── spdlog_vendor
├── sros2
├── system_tests
├── test_interface_files
├── tinyxml2_vendor
├── tinyxml_vendor
├── tlsf
├── unique_identifier_msgs
├── urdf
├── variants
└── yaml_cpp_vendor
```

#### Installation

1.  Download RPM

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rosbase/ros-foxy-ros-base-0.9.2-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rosbase/ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```

2.  Install RPM

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-base-0.9.2-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```

#### Instructions

Dependence installation:

```
sh /opt/ros/foxy/install_dependence.sh
```

Exit the following output file under the /opt/ros/foxy/ directory,Prove that the software installation is successful.

output:

```
./
├── bin
├── cmake
├── COLCON_IGNORE
├── include
├── install_dependence.sh
├── lib
├── lib64
├── local_setup.bash
├── local_setup.ps1
├── local_setup.sh
├── _local_setup_util_ps1.py
├── _local_setup_util_sh.py
├── local_setup.zsh
├── opt
│   └── yaml_cpp_vendor
├── setup.bash
├── setup.ps1
├── setup.sh
├── setup.zsh
├── share
├── src
│   ├── gmock_vendor
│   └── gtest_vendor
└── tools
    └── fastdds
```
#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
