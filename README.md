# ros2_base

#### 介绍
The basic software package of ros2.

#### 软件架构
软件架构说明

https://github.com/ros2-gbp/variants-release.git

文件内容:
```
./
├── ament_cmake_ros
├── common_interfaces
├── console_bridge_vendor
├── eigen3_cmake_module
├── example_interfaces
├── geometry2
├── launch
├── launch_ros
├── libyaml_vendor
├── message_filters
├── mimick_vendor
├── orocos_kinematics_dynamics
├── performance_test_fixture
├── python_cmake_module
├── rcl
├── rclcpp
├── rcl_interfaces
├── rcl_logging
├── rclpy
├── rcpputils
├── rcutils
├── realtime_support
├── rmw
├── rmw_connext
├── rmw_cyclonedds
├── rmw_dds_common
├── rmw_fastrtps
├── rmw_implementation
├── ros1_bridge
├── ros2cli
├── ros2cli_common_extensions
├── rosbag2
├── rosidl
├── rosidl_dds
├── rosidl_defaults
├── rosidl_python
├── rosidl_runtime_py
├── rosidl_typesupport
├── rosidl_typesupport_connext
├── rosidl_typesupport_fastrtps
├── ros_testing
├── rpyutils
├── spdlog_vendor
├── sros2
├── system_tests
├── test_interface_files
├── tinyxml2_vendor
├── tinyxml_vendor
├── tlsf
├── unique_identifier_msgs
├── urdf
├── variants
└── yaml_cpp_vendor
```

#### 安装教程

1. 下载rpm包

aarch64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_aarch64/aarch64/rosbase/ros-foxy-ros-base-0.9.2-2.oe2203.aarch64.rpm
```

x86_64:

```
wget https://117.78.1.88/build/home:Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/rosbase/ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```

2. 安装rpm包

aarch64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-base-0.9.2-2.oe2203.aarch64.rpm
```

x86_64:

```
sudo rpm -ivh --nodeps --force ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```

#### 使用说明

依赖环境安装:

```
sh /opt/ros/foxy/install_dependence.sh
```

安装完成以后，在/opt/ros/foxy/目录下如下输出,则表示安装成功。

输出:

```
./
├── bin
├── cmake
├── COLCON_IGNORE
├── include
├── install_dependence.sh
├── lib
├── lib64
├── local_setup.bash
├── local_setup.ps1
├── local_setup.sh
├── _local_setup_util_ps1.py
├── _local_setup_util_sh.py
├── local_setup.zsh
├── opt
│   └── yaml_cpp_vendor
├── setup.bash
├── setup.ps1
├── setup.sh
├── setup.zsh
├── share
├── src
│   ├── gmock_vendor
│   └── gtest_vendor
└── tools
    └── fastdds
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
