Name:		ros-foxy-ros-base
Version:	0.9.2
Release:	2
Summary:	This is ROS2 foxy %{name} Package
License:	Public Domain and Apache-2.0 and BSD and MIT and BSL-1.0 and LGPL-2.1-only and MPL-2.0 and GPL-3.0-only and GPL-2.0-or-later and MPL-1.1 and IJG and Zlib and OFL-1.1
URL:		https://github.com/ros2-gbp/variants-release.git
Source0:	https://github.com/ros2-gbp/variants-release/archive/release/foxy/ros_base/0.9.2-1.tar.gz
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pytest
BuildRequires:	asio-devel
BuildRequires:	tinyxml2-devel
BuildRequires:	git
BuildRequires:	qt5-devel
BuildRequires:	bullet-devel
BuildRequires:  zstd-devel
BuildRequires:  libzstd-devel
BuildRequires:  openblas-devel

%description
This is ROS2 foxy %{name} Package.

%prep
%setup

%install

# for colcon build tools
#cd build_tools
#export BUILD_WORSPCE=$PWD
#./colcon/colcon-core/bin/colcon build --paths colcon/* --merge-install
#source install/local_setup.sh
#cd ..

# for workspace
#cd workspace
#colcon build --merge-install
#cp ../install_dependence.sh install/
#cp -r ../build_tools/install/bin install/
#cp -r ../build_tools/install/lib install/
#cp -r ../build_tools/install/share install/

####
# for unit test 循环拷贝所有的东西
#
####

####
# 对install内部的变量名称进行替换
#
####
#SRC_PATH=$PWD/install
#DST_PATH=/opt/ros/foxy
#sed -i "s:${SRC_PATH}:${DST_PATH}:g"  `grep -rIln "${SRC_PATH}" install/*`

#SRC_PATH=$BUILD_WORSPCE/install
#DST_PATH=/opt/ros/foxy
#sed -i "s:${SRC_PATH}:${DST_PATH}:g"  `grep -rIln "${SRC_PATH}" install/*`

####
# install
#
####
#mkdir -p %{buildroot}/opt/ros/foxy/
#cp -r install/* %{buildroot}/opt/ros/foxy/

###for debug
#mkdir -p %{buildroot}/opt/ros/foxy/log
#cp -r log/ %{buildroot}/opt/ros/foxy/log

%files
%defattr(-,root,root)

%changelog
* Sat Nov 19 2022 openEuler Buildteam <hanhaomin008@126.com> - 0.9.2-2
- Fix Package name
* Wed Sep 21 2022 openEuler Buildteam <hanhaomin008@126.com> - 0.9.2-1
- Update Package to OpenEuler 22.03
